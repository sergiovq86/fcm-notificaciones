package com.estech.testingfcm;

import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Método llamado cuando se recibe un mensaje
     *
     * @param remoteMessage Objeto que representa el mensaje recibido de FCM
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        /* Hya dos tipos de mensajes: mensajes de dataos y mensajes de notificación.
        Los mensajes de datos se pueden gestionar aquí si la app está en background o foreground.
        Los mensajes de notificación son recibidos aquí en foreground. Si la App está en background
        se autogenera una n otificación.
         */

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // comprueba si el mensaje es de datos
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

        }

        // Compruebab si el mensaje es una notificación
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

            Map<String, String> map = remoteMessage.getData();
            String nombre = map.get("nombre");
            broadcastUpdate(remoteMessage.getNotification());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void broadcastUpdate(RemoteMessage.Notification notification) {
        Intent intent = new Intent(MainActivity.ACTION_DATA_AVAILABLE);
        intent.putExtra("titulo", notification.getTitle());
        intent.putExtra("msg", notification.getBody());

        sendBroadcast(intent);
    }

    // [START on_new_token]

    /**
     * Llamado cuando el token es refrescado
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        sendRegistrationToServer(token);
    }
    // [END on_new_token]


    /**
     * Método para mandar el token al servidor
     *
     * @param token nuevo token
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }

    /**
     * Notificación simple
     *
     * @param titulo titulo a mostrar
     * @param body   cuerpo del mensaje recibido en el FCM
     */
    private void sendNotification(String titulo, String body) {
        Intent notifyIntent = new Intent(this, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notifyIntent.putExtra("msg", "Abierto desde una notificación");
        PendingIntent notifyPending = PendingIntent.getActivity(
                this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
        );

        String channelId = getString(R.string.default_notification_channel_id);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_sunny)
                        .setContentTitle(titulo)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setContentIntent(notifyPending);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(0, builder.build());

    }
}
